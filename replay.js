var sinhVienARR = [];

// LAY THONG TIN TU LOCAL
const SinhVien_LoCal = "SinhVien_Local";
var dataLocal = localStorage.getItem(SinhVien_LoCal);
if (dataLocal != null) {
  sinhVienARR = JSON.parse(dataLocal);
  renderDSSV(sinhVienARR);
}

// THEM SINH VIEN
function themSV() {
  var sinhVien = layThongTinTuForm();
  sinhVienARR.push(sinhVien);
  console.log(sinhVienARR);
  renderDSSV(sinhVienARR);

  // LOCAL STORAGE
  setLocal();
  //RESET
  reset();
}

//LOCAL STORAGE
function setLocal() {
  var dataLocal = JSON.stringify(sinhVienARR);
  localStorage.setItem(SinhVien_LoCal, dataLocal);
}

// RENDER SV HTML
function renderDSSV(sinhVienARR) {
  var contentHTML = "";
  for (var i = 0; i < sinhVienARR.length; i++) {
    var contentSV = sinhVienARR[i];
    contentHTML += `<tr>
    <td>${contentSV.maAR}</td>
    <td>${contentSV.tenAR}</td>
    <td>${contentSV.emailAR}</td>
    <td>${tinhDiemTrungBinh(
      contentSV.diemToanAR,
      contentSV.diemHoaAR,
      contentSV.diemLyAR
    )}</td>
    <td><button class="btn btn-danger" onclick="xoaSV(${
      contentSV.maAR
    })">Xoá</button>
    <button class="btn btn-success" onclick="suaSV(${
      contentSV.maAR
    })">Sửa</button></td>
    </tr>`;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

// RESET
function reset() {
  document.getElementById("formQLSV").reset();
}

// RESET FORM INPUT
function resetForm() {
  sinhVienARR.length = 0;
  renderDSSV(sinhVienARR);
  reset();
  setLocal();
}

// TINH DIEM TRUNG BINH
function tinhDiemTrungBinh(a, b, c) {
  var tong = (parseFloat(a) + parseFloat(b) + parseFloat(c)) / 3;
  var tongFix = tong.toFixed(2);
  console.log(tongFix);
  return tongFix;
}

// XOA SINH VIEN
function xoaSV(id) {
  for (var i = 0; i < sinhVienARR.length; i++) {
    var viTri = -1;
    if (sinhVienARR[i].maAR == id) {
      viTri = i;
      console.log(viTri);
    }
  }
  if (viTri != -1) {
    sinhVienARR.splice(viTri, 1);
    renderDSSV(sinhVienARR);
    setLocal();
  }
}

//LAY THONG TIN TU FORM
function layThongTinTuForm() {
  var valueMaSV = document.getElementById("txtMaSV").value;
  var valueTenSV = document.getElementById("txtTenSV").value;
  var valueEmailSV = document.getElementById("txtEmail").value;
  var valuePassSV = document.getElementById("txtPass").value;
  var valueToanSV = document.getElementById("txtDiemToan").value * 1;
  var valueLySV = document.getElementById("txtDiemLy").value * 1;
  var valueHoaSV = document.getElementById("txtDiemHoa").value * 1;
  var sinhVien = {
    maAR: valueMaSV,
    tenAR: valueTenSV,
    emailAR: valueEmailSV,
    passAR: valuePassSV,
    diemToanAR: valueToanSV,
    diemLyAR: valueLySV,
    diemHoaAR: valueHoaSV,
  };
  return sinhVien;
}

// SUA SINH VIEN
function suaSV(id) {
  var viTri = sinhVienARR.findIndex(function (contentSV) {
    return contentSV.maAR == id;
  });
  var showThongtin = sinhVienARR[viTri];
  document.getElementById("txtMaSV").value = showThongtin.maAR;
  document.getElementById("txtMaSV").disabled = true;
  document.getElementById("txtTenSV").value = showThongtin.tenAR;
  document.getElementById("txtEmail").value = showThongtin.emailAR;
  document.getElementById("txtPass").value = showThongtin.passAR;
  document.getElementById("txtDiemToan").value = showThongtin.diemToanAR;
  document.getElementById("txtDiemLy").value = showThongtin.diemLyAR;
  document.getElementById("txtDiemHoa").value = showThongtin.diemHoaAR;
}

//CAP NHAT
function capNhatSV() {
  var sinhVien = layThongTinTuForm();
  var viTri = sinhVienARR.findIndex(function (contentSV) {
    return contentSV.maAR == sinhVien.maAR;
  });
  sinhVienARR[viTri] = sinhVien;
  renderDSSV(sinhVienARR);
  setLocal();
}

// SEARCH
function searchItem() {
  var resultSearch = [];
  var infor = document.getElementById("txtSearch").value.trim();
  var viTri = sinhVienARR.findIndex(function (item) {
    return item.tenAR.toLowerCase() === infor.toLowerCase();
  });
  resultSearch.push(sinhVienARR[viTri]);
  renderDSSV(resultSearch);
}

// SHOW MESSAGE
function showMessage(idTag, message) {
  document.getElementById(idTag).innerHTML = message;
}
